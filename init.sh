#!/bin/fish
# you need to have kubectl installed and configured

# deploy argocd 
kubectl create namespace argocd
kubectl apply -n argocd -f argocd-install.yaml

kubectl patch svc argocd-server -n argocd -p '{"spec": {"type": "LoadBalancer"}}'

# deploy argocd application
kubectl apply -n argocd -f application.yaml


# init dashboard
kubectl apply -f recommended.yaml 

# init dashboard-user
kubectl apply -f dashboard-user.yaml
