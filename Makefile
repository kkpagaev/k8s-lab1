
# Gets argo password 
argo:
	@./scripts/get-argo-pwd.sh

# Get the token for the dashboard
dash:
	@./scripts/get-dashboard-token.sh